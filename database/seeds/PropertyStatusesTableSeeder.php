<?php

use Illuminate\Database\Seeder;

class PropertyStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $propertyStatuses = [
        	'Active',
        	'Active Under Contract',
        	'Active with 24/48 hour Right of Refusal',
        	'Closed',
        	'Expired',
    		'New', 
        	'Pending',
    		'Sold',
        	'Temporarily Off Market',
        	'Under Contract',
    		'Under Construction', 
    		'Under Renovation'
    	];

    	foreach ($propertyStatuses as $key => $value) {
    		DB::table('property_statuses')->insert([
	            'name' => $value,
	            'created_at' => date("Y-m-d H:i:s"),
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
