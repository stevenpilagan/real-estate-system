<?php

use Illuminate\Database\Seeder;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$projectTypes = [
    		'Commercial',
    		'Industrial',
    		'Residential',
    		'Sub-division',
    	];

    	foreach ($projectTypes as $key => $value) {
    		DB::table('project_types')->insert([
	            'name' => $value,
	            'created_at' => date("Y-m-d H:i:s"),
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
