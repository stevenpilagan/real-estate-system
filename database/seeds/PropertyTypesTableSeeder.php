<?php

use Illuminate\Database\Seeder;

class PropertyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $propertyTypes = [
    		'Apartments', 
    		'Commercial', 
    		'Condominium', 
    		'Mansion', 
    		'Studio', 
    		'Townhouse', 
    		'Subdivision', 
    		'Villa'
    	];

    	foreach ($propertyTypes as $key => $value) {
    		DB::table('property_types')->insert([
	            'name' => $value,
	            'created_at' => date("Y-m-d H:i:s"),
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
