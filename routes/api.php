<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('get/')->group(function () {
	Route::prefix('project/')->group(function () {
		Route::get('all', 'ProjectController@getall');
		Route::get('id/{id}', 'ProjectController@getById');
		Route::get('location/{location}', 'ProjectController@getByLocation');
		Route::get('name/{name}', 'ProjectController@getByName');
		Route::get('status/{status}', 'ProjectController@getByStatus');
	    Route::get('statuses/', 'ProjectStatusController@getStatuses');
	});

	Route::prefix('property/')->group(function () {
		Route::get('all', 'PropertyController@getall');
		Route::get('id/{id}', 'PropertyController@getById');
		Route::get('location/{location}', 'PropertyController@getByLocation');
		Route::get('name/{name}', 'PropertyController@getByName');
		Route::get('status/{status}', 'PropertyController@getByStatus');
	    Route::get('statuses', 'PropertyStatusController@getStatuses');
	});
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
