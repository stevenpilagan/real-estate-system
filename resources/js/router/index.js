import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../components/pages/home.vue'
import Projects from '../components/pages/projects.vue'
import ProjectInfo from '../components/pages/projectInfo.vue'
import Properties from '../components/pages/properties.vue'
import PropertyInfo from '../components/pages/propertyInfo.vue'
import Search from '../components/pages/search.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Home
        },
        {
            path: '/home',
            name: 'Home',
            component: Home
        },
        {
            path: '/projects',
            name: 'Projects',
            component: Projects
        },
        {
            path: '/project/id/:id/name/:name',
            name: 'Project',
            component: ProjectInfo,
            props: true
        },
        {
            path: '/properties',
            name: 'Properties',
            component: Properties,
        },
        {
            path: '/property/id/:id/name/:name',
            name: 'Property',
            component: PropertyInfo,
            props: true
        },
        {
            path: '/search',
            name: 'Search',
            component: Search
        }
    ]
})