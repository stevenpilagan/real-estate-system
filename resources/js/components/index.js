Vue.component('appContainer', require('./appContainer.vue'));
Vue.component('mainNav', require('./sections/mainNav.vue'));
Vue.component('footerNav', require('./sections/footerNav.vue'));
Vue.component('searchBar', require('./sections/searchBar.vue'));
Vue.component('sortBar', require('./sections/sortBar.vue'));