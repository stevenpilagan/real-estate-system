require('./bootstrap');
window.Vue = require('vue')

import router from './router'
import store from './store'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
require('./components')

const app = new Vue({
    el: '#app',
    router,
    store
})
