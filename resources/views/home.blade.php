<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Real Estate</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet"> 
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        <div id="app">
            <app-container></app-container>
        </div>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
