<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;

class ProjectController extends Controller
{
	/**
	 * [getAll description]
	 * @return [type] [description]
	 */
    public function getAll() {
    	$projects = Project::getAll();

    	if( count($projects) > 0 ) {
    		return response()->json($projects);
    	}
    	else {
    		return ['message' => 'No available projects yet'];
    	}
    }

	/**
	 * [getAll description]
	 * @return [type] [description]
	 */
    public function getById( $id ) {
    	return 'get by id '.$id;
    }

    /**
     * [getAll description]
     * @return [type] [description]
     */
    public function getByStatus( $status ) {
    	return 'get by status '.$status;
    }

    /**
     * [getAll description]
     * @return [type] [description]
     */
    public function getByLocation( $location ) {
    	return 'get by location '.$location;
    }

    /**
     * [getAll description]
     * @return [type] [description]
     */
    public function getByName( $name ) {
    	return 'get by name '.$name;
    }
}
