<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

class PropertyController extends Controller
{
	/**
	 * [getAll description]
	 * @return [type] [description]
	 */
    public function getAll() {
    	$properties = Property::getAll();

    	if( count($properties) > 0 ) {
    		return response()->json($properties);
    	}
    	else {
    		return ['message' => 'No available properties yet'];
    	}
    }

    /**
     * [getById description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getById( $id ) {
    	return 'get by id '.$id;
    }

    /**
     * [getByStatus description]
     * @param  [type] $status [description]
     * @return [type]         [description]
     */
    public function getByStatus( $status ) {
    	return 'get by status '.$status;
    }

    /**
     * [getByLocation description]
     * @param  [type] $location [description]
     * @return [type]           [description]
     */
    public function getByLocation( $location ) {
    	return 'get by location '.$location;
    }

    /**
     * [getByName description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public function getByName( $name ) {
    	return 'get by name '.$name;
    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function create() {

    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function update() {

    }

    /**
     * [create description]
     * @return [type] [description]
     */
    public function delete() {

    }
}
