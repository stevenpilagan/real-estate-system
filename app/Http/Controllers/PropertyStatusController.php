<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyStatus;

class PropertyStatusController extends Controller
{
    public function getStatuses() {
    	return PropertyStatus::getStatuses();
    }
}
