<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectStatus;

class ProjectStatusController extends Controller
{
    public function getStatuses() {
    	return ProjectStatus::getStatuses();
    }
}
