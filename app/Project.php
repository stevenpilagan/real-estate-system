<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public static function getAll() {
    	return [
    		[
    			'id' => 1,
    			'name' => '81st Executive Tower',
    			'street' => '11th street South Corner',
				'city' => 'NY',
				'state' => null,
				'country' => 'US',
				'lat' => '40.741895',
				'lon' => '-73.989308',
				'project_info' =>  '<h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a { display: block; width: 300px; height: 80px; }</code></pre>',
				'bed' => 3,
				'carpark' => 2,
				'dining' => 1,
				'bathroom' => 1,
				'livingroom' => 1,
				'pool' => 1,
				'area' => '250 sq.ft',
				'layout' => [
					'1 Bedroom', '2 Bedroom', 'Studio', 'High Rise'
				],
				'floorplan' => 'http://static1.1.sqspcdn.com/static/p/1102374/17040217/1354844281927/open-plan-1.png?asGalleryImage=true&token=GU3206NQ23rGW1LgV3ZcgfgxRhU%3D',
				'developer' => 'Project Developer Inc.',
				'agent' =>  'Robert Herrelson',
				'images' => [
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-view-Hi-res.jpg',
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-apt-Hi-res.jpg',
					'https://images.pexels.com/photos/534182/pexels-photo-534182.jpeg?cs=srgb&dl=architecture-balcony-building-534182.jpg&fm=jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705875/3V1A5775.0.jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705877/3V1A5732.0.jpg',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlBCWsTZsIY9VQS5AydBekJPOV0AozbwIMzN7YXDoJqusZ--Q2',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7rtWLeos0Pgz7piCbhb3cq6tlT6yCqMXVNlzw1pqX-UE5cjBoMw',
				]
    		],
    		[
    			'id' => 2,
    			'name' => '81st Executive Tower',
    			'street' => '11th street South Corner',
				'city' => 'NY',
				'state' => null,
				'country' => 'US',
				'lat' => '40.741895',
				'lon' => '-73.989308',
				'project_info' =>  '<h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a { display: block; width: 300px; height: 80px; }</code></pre>',
				'bed' => 3,
				'carpark' => 2,
				'dining' => 1,
				'bathroom' => 1,
				'livingroom' => 1,
				'pool' => 1,
				'area' => '250 sq.ft',
				'layout' => [
					'1 Bedroom', '2 Bedroom', 'Studio', 'High Rise'
				],
				'floorplan' => 'http://static1.1.sqspcdn.com/static/p/1102374/17040217/1354844281927/open-plan-1.png?asGalleryImage=true&token=GU3206NQ23rGW1LgV3ZcgfgxRhU%3D',
				'developer' => 'Project Developer Inc.',
				'agent' =>  'Robert Herrelson',
				'images' => [
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-view-Hi-res.jpg',
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-apt-Hi-res.jpg',
					'https://images.pexels.com/photos/534182/pexels-photo-534182.jpeg?cs=srgb&dl=architecture-balcony-building-534182.jpg&fm=jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705875/3V1A5775.0.jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705877/3V1A5732.0.jpg',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlBCWsTZsIY9VQS5AydBekJPOV0AozbwIMzN7YXDoJqusZ--Q2',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7rtWLeos0Pgz7piCbhb3cq6tlT6yCqMXVNlzw1pqX-UE5cjBoMw',
				]
    		],
    		[
    			'id' => 3,
    			'name' => '81st Executive Tower',
    			'street' => '11th street South Corner',
				'city' => 'NY',
				'state' => null,
				'country' => 'US',
				'lat' => '40.741895',
				'lon' => '-73.989308',
				'project_info' =>  '<h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a { display: block; width: 300px; height: 80px; }</code></pre>',
				'bed' => 3,
				'carpark' => 2,
				'dining' => 1,
				'bathroom' => 1,
				'livingroom' => 1,
				'pool' => 1,
				'area' => '250 sq.ft',
				'layout' => [
					'1 Bedroom', '2 Bedroom', 'Studio', 'High Rise'
				],
				'floorplan' => 'http://static1.1.sqspcdn.com/static/p/1102374/17040217/1354844281927/open-plan-1.png?asGalleryImage=true&token=GU3206NQ23rGW1LgV3ZcgfgxRhU%3D',
				'developer' => 'Project Developer Inc.',
				'agent' =>  'Robert Herrelson',
				'images' => [
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-view-Hi-res.jpg',
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-apt-Hi-res.jpg',
					'https://images.pexels.com/photos/534182/pexels-photo-534182.jpeg?cs=srgb&dl=architecture-balcony-building-534182.jpg&fm=jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705875/3V1A5775.0.jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705877/3V1A5732.0.jpg',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlBCWsTZsIY9VQS5AydBekJPOV0AozbwIMzN7YXDoJqusZ--Q2',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7rtWLeos0Pgz7piCbhb3cq6tlT6yCqMXVNlzw1pqX-UE5cjBoMw',
				]
    		],
    		[
    			'id' => 4,
    			'name' => '81st Executive Tower',
    			'street' => '11th street South Corner',
				'city' => 'NY',
				'state' => null,
				'country' => 'US',
				'lat' => '40.741895',
				'lon' => '-73.989308',
				'project_info' =>  '<h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a { display: block; width: 300px; height: 80px; }</code></pre>',
				'bed' => 3,
				'carpark' => 2,
				'dining' => 1,
				'bathroom' => 1,
				'livingroom' => 1,
				'pool' => 1,
				'area' => '250 sq.ft',
				'layout' => [
					'1 Bedroom', '2 Bedroom', 'Studio', 'High Rise'
				],
				'floorplan' => 'http://static1.1.sqspcdn.com/static/p/1102374/17040217/1354844281927/open-plan-1.png?asGalleryImage=true&token=GU3206NQ23rGW1LgV3ZcgfgxRhU%3D',
				'developer' => 'Project Developer Inc.',
				'agent' =>  'Robert Herrelson',
				'images' => [
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-view-Hi-res.jpg',
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-apt-Hi-res.jpg',
					'https://images.pexels.com/photos/534182/pexels-photo-534182.jpeg?cs=srgb&dl=architecture-balcony-building-534182.jpg&fm=jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705875/3V1A5775.0.jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705877/3V1A5732.0.jpg',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlBCWsTZsIY9VQS5AydBekJPOV0AozbwIMzN7YXDoJqusZ--Q2',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7rtWLeos0Pgz7piCbhb3cq6tlT6yCqMXVNlzw1pqX-UE5cjBoMw',
				]
    		],
    		[
    			'id' => 5,
    			'name' => '81st Executive Tower',
    			'street' => '11th street South Corner',
				'city' => 'NY',
				'state' => null,
				'country' => 'US',
				'lat' => '40.741895',
				'lon' => '-73.989308',
				'project_info' =>  '<h1>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h1><p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p><h2>Header Level 2</h2><ol><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ol><blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote><h3>Header Level 3</h3><ul><li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li><li>Aliquam tincidunt mauris eu risus.</li></ul><pre><code>#header h1 a { display: block; width: 300px; height: 80px; }</code></pre>',
				'bed' => 3,
				'carpark' => 2,
				'dining' => 1,
				'bathroom' => 1,
				'livingroom' => 1,
				'pool' => 1,
				'area' => '250 sq.ft',
				'layout' => [
					'1 Bedroom', '2 Bedroom', 'Studio', 'High Rise'
				],
				'floorplan' => 'http://static1.1.sqspcdn.com/static/p/1102374/17040217/1354844281927/open-plan-1.png?asGalleryImage=true&token=GU3206NQ23rGW1LgV3ZcgfgxRhU%3D',
				'developer' => 'Project Developer Inc.',
				'agent' =>  'Robert Herrelson',
				'images' => [
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-view-Hi-res.jpg',
					'https://www.flatironre.com/wp-content/uploads/2017/11/Full-apt-Hi-res.jpg',
					'https://images.pexels.com/photos/534182/pexels-photo-534182.jpeg?cs=srgb&dl=architecture-balcony-building-534182.jpg&fm=jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705875/3V1A5775.0.jpg',
					'https://cdn.vox-cdn.com/uploads/chorus_asset/file/6705877/3V1A5732.0.jpg',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlBCWsTZsIY9VQS5AydBekJPOV0AozbwIMzN7YXDoJqusZ--Q2',
					'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7rtWLeos0Pgz7piCbhb3cq6tlT6yCqMXVNlzw1pqX-UE5cjBoMw',
				]
    		]
    	];
    	//return self::all();
    }
}
