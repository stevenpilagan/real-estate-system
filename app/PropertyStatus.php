<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyStatus extends Model
{
	protected $hidden = [
        'created_at', 'updated_at',
    ];
    
    public static function getStatuses() {
    	return self::all();
    }
}
